# The HappyMe Project

## Our Mission
##### Written by Ellie Archer, original team member
Our Aim at HappyMe is to create a safe and private place that people can report any issues they might be facing. We strive to ensure that all the people and Radford feel like they have someone to talk to. We think we have found a simple way that someone can report things that have happened or that are happening. We also provide contacts to councillors, here at Radford, and links to help lines and other bullying reporting apps. HappyMe is an app (also available online) where students can anonymously report things like bullying, issues at home, friendship problems and more.

 

At HappyMe if you select the option to be anonymous your information will be kept a secret, even from us! Your chats and reported issues have the option to also be anonymous! If you are happy for us to know who you are that is okay too!

 

The features of HappyMe is a reporting system, live chat, general advice from experts and a way to contact a counciler here at Radford. HappyMe has also created a website for those who do not have access to the Appstore or prefer not to download the app.

## Our Team

- Will Pak Poy - Developer (@pakkie64)
- Jack Prime - Developer (@eddster2309)
- George Birmingham - Part-time developer

<details>
  <summary>Previous Members</summary>

**Round 1**
- Ellie Archer
- Palak Maheshwari
- Ella Southwell
- Hamish Gaden
- Hamish McKee

**Round 2**
- Josie Holt
- Charlie Seddon
- Sophie Hall
</details>

---

## Folder Structure
<details>
<summary>Archived Folder Structure</summary>

**No longer maintained**
- Assets: Assets used in the creation of the app.
- DevPlayground: Testing of new SwiftUI features visible to everyone.
- HappyMe: Original app designed using Interface Builder and UIKit. **Archived**
- ProjectHappy: SwiftUI Project V1  **Archived**
- Swifty: Backups (not used)
- HMD2: SwiftUI Project Versos 2 **Latest**

</details>

---

## Technical
HappyMe is currently using Apache Cordova. Cordova allows us to use industry-standard web components to create apps for iOS, Android, Windows and macOS. We are also using SQLite for databases, and use Microsoft Azure AD for authentication.
